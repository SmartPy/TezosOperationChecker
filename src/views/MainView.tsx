import * as React from 'react';

import { ThemeProvider } from '@material-ui/core/styles';
import { createTheme } from '@material-ui/core/styles'
import {
    CssBaseline,
    TextField,
    Typography,
    AppBar,
    Toolbar,
    IconButton,
    Tooltip,
    Paper,
    Button,
} from '@material-ui/core';

import DeleteIcon from '@material-ui/icons/Delete';
import Light from '@material-ui/icons/Brightness7';
import Dark from '@material-ui/icons/Brightness4';

import { keys } from '../storage';

import './MainView.css';
import Logo from '../components/icons/SmartPyLogo';
import { generateBlake2BHash, getEncoding } from '../utils/misc';

const themeInStorage = window.localStorage.getItem(keys.THEME);

const MainView: React.FC = () => {
    const [encoding, setEncoding] = React.useState('');
    const [blake2BHash, setBlake2BHash] = React.useState('');
    // true = light, false = dark (default is true)
    const [themeMode, setThemeMode] = React.useState(
        !themeInStorage || themeInStorage === 'light' || false
    );
    // Block Header
    const [blockHeader, setBlockHeader] = React.useState(
        window.localStorage.getItem(keys.BLOCK_HEADER) || ''
    );
    // Operations contents
    const [operations, setOperations] = React.useState(
        window.localStorage.getItem(keys.OPERATIONS_CONTENT) || ''
    );

    /**
     * Updates color palette and the local storage every themeMode state changes.
     */
    const theme = React.useMemo(() => {
        // Update Storage
        window.localStorage.setItem(keys.THEME, themeMode ? 'light' : 'dark');
        // Update palette
        return createTheme({
            palette: {
                type: themeMode ? 'light' : 'dark',
            },
        });
    }, [themeMode]);

    React.useEffect(() => {
        const hex = getEncoding(operations, blockHeader);
        if (hex) {
            setEncoding(hex);
            setBlake2BHash(generateBlake2BHash(hex));
        } else {
            setEncoding('');
            setBlake2BHash('');
        }
    }, [operations, blockHeader]);

    /**
     * Handles theme change when the user clicks in the toggle button.
     */
    const handleColorToggle = () => {
        setThemeMode((prevState) => !prevState);
    };

    /**
     * Remove blockHeader and operations keys from the storage.
     */
    const clearInputsFromStorage = () => {
        setBlockHeader('');
        setOperations('');
        window.localStorage.removeItem(keys.BLOCK_HEADER);
        window.localStorage.removeItem(keys.OPERATIONS_CONTENT);
    };

    const handleBlockHeaderChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setBlockHeader(event.target.value);
        window.localStorage.setItem(keys.BLOCK_HEADER, event.target.value);
    };

    const handleOperationsChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        let operations: any = event.target.value;
        try {
            operations = JSON.parse(event.target.value);
            if (!Array.isArray(operations)) {
                operations = [operations];
            }
            operations = JSON.stringify(operations, null, 4);
        } catch (e) {
            console.error(e);
        }

        setOperations(operations);
        window.localStorage.setItem(
            keys.OPERATIONS_CONTENT,
            event.target.value
        );
    };

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <AppBar position="static" color="inherit">
                <Toolbar className="toolbar">
                    <Tooltip
                        title="Toggle Light/Dark Theme"
                        aria-label="theme"
                        placement="right"
                    >
                        <IconButton
                            aria-label="theme"
                            size="small"
                            onClick={handleColorToggle}
                        >
                            {themeMode ? <Light /> : <Dark />}
                        </IconButton>
                    </Tooltip>
                    <Typography variant="h6" component="h2">
                        TezosOperationChecker
                    </Typography>
                    <Tooltip title="Clear" aria-label="clear" placement="left">
                        <IconButton
                            aria-label="clear"
                            size="small"
                            onClick={clearInputsFromStorage}
                        >
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                </Toolbar>
            </AppBar>

            <div className="App">
                <Typography variant="caption" component="h2" color="error">
                    <div>
                        {blockHeader && operations && !encoding
                            ? 'The inputs are invalid!'
                            : ''}
                    </div>
                </Typography>
                <div className="inputSection">
                    <div className="input">
                        <TextField
                            value={blockHeader}
                            className="input"
                            label="Block Header"
                            placeholder="Place the block header here..."
                            fullWidth
                            multiline
                            variant="outlined"
                            onChange={handleBlockHeaderChange}
                        />
                    </div>
                    <div className="input">
                        <TextField
                            value={operations}
                            label="Operations"
                            placeholder="Place the operations here..."
                            fullWidth
                            multiline
                            rows={10}
                            variant="outlined"
                            onChange={handleOperationsChange}
                        />
                    </div>
                </div>
                <div>
                    <div className="input">
                        <TextField
                            disabled
                            label="Blake 2b Hash"
                            fullWidth
                            multiline
                            maxRows={2}
                            value={blake2BHash}
                            variant="filled"
                        />
                    </div>
                    <div className="input">
                        <TextField
                            disabled
                            label="Encoding (Locally Forged)"
                            fullWidth
                            multiline
                            value={encoding}
                            variant="filled"
                            maxRows={5}
                            color="secondary"
                        />
                    </div>
                </div>
            </div>

            <Paper square className="footer">
                By
                <Button href="https://smartpy.io" target="_blank">
                    <Logo />
                </Button>
            </Paper>
        </ThemeProvider>
    );
};

export default MainView;
