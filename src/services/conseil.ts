import { keywords } from '../utils/michelson';
import { TezosLanguageUtil } from 'conseiljs';

TezosLanguageUtil.overrideKeywordList(keywords);

export * from 'conseiljs';
export * from 'conseiljs/dist/chain/tezos/TezosMessageCodec';
