import * as React from 'react';
import { render } from 'react-dom';

// Local imports
import MainView from '../../views/MainView';

import './index.css';

render(<MainView />, window.document.querySelector('#app-container'));
