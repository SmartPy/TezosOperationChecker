import * as React from 'react';
import { render } from 'react-dom';
import {
    Dialog,
    DialogContent,
    DialogTitle,
    Divider,
    Grid,
    Paper,
    PaperProps,
    TextField,
    ThemeProvider,
} from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles'
import ScopedCssBaseline from '@material-ui/core/ScopedCssBaseline';
import Draggable from 'react-draggable';

import { keys } from '../../storage';
import { generateBlake2BHash, getEncoding } from '../../utils/misc';
import '../../assets/img/icon-36.png';
import '../../assets/img/icon-128.png';

const theme = createTheme({
    palette: {
        type: 'light',
    },
});

enum MessageKind {
    Request = 'REQUEST',
}

interface Payload {
    blockHash: string;
    operations: any;
}
interface Message {
    kind: MessageKind;
    payload: Payload;
}

function PaperComponent(props: PaperProps) {
    return (
        <Draggable
            handle="#tezos-operation-checker"
            cancel={'[class*="MuiDialogContent-root"]'}
        >
            <Paper {...props} />
        </Draggable>
    );
}

function DraggableDialog() {
    const [open, setOpen] = React.useState(false);
    const [encoding, setEncoding] = React.useState('');
    const [blake2BHash, setBlake2BHash] = React.useState('');
    // Block Header
    const [blockHeader, setBlockHeader] = React.useState(
        window.localStorage.getItem(keys.BLOCK_HEADER) || ''
    );
    // Operations contents
    const [operations, setOperations] = React.useState(
        window.localStorage.getItem(keys.OPERATIONS_CONTENT) || ''
    );

    // Add event listener
    React.useEffect(() => {
        const handler = (event) => {
            // Only accept messages from the same frame and targetting TezosOperationChecker extension
            if (
                event.source !== window ||
                event.data.source !== 'TezosOperationChecker'
            ) {
                return;
            }
            const message: Message = event.data;

            // Open dialog
            setOpen(true);

            switch (message?.kind) {
                case MessageKind.Request:
                    setOperations(
                        JSON.stringify(message.payload.operations, null, 4)
                    );
                    setBlockHeader(message.payload.blockHash);
            }

        };
        window.addEventListener('message', handler);

        return () => {
            window.removeEventListener('message', handler);
        };
    }, []);

    React.useEffect(() => {
        const hex = getEncoding(operations, blockHeader);
        if (hex) {
            setEncoding(hex);
            setBlake2BHash(generateBlake2BHash(hex));
        } else {
            setEncoding('');
            setBlake2BHash('');
        }
    }, [operations, blockHeader]);

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <ThemeProvider theme={theme}>
            <ScopedCssBaseline>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    PaperComponent={PaperComponent}
                    aria-labelledby="tezos-operation-checker"
                    fullWidth
                >
                    <DialogTitle
                        style={{ cursor: 'move' }}
                        id="tezos-operation-checker"
                    />
                    <DialogContent>
                        <Grid container direction="column">
                            <Grid item>
                                <TextField
                                    value={blockHeader}
                                    className="input"
                                    label="Block Header"
                                    fullWidth
                                    multiline
                                    variant="outlined"
                                    disabled
                                />
                            </Grid>
                            <Divider flexItem style={{ margin: 5 }}/>
                            <Grid item>
                                <TextField
                                    value={operations}
                                    label="Operations"
                                    fullWidth
                                    multiline
                                    rows={10}
                                    variant="outlined"
                                    disabled
                                />
                            </Grid>
                            <Divider flexItem style={{ margin: 5 }}/>
                            <Grid item>
                                <TextField
                                    disabled
                                    label="Blake 2b Hash"
                                    fullWidth
                                    multiline
                                    maxRows={2}
                                    value={blake2BHash}
                                    variant="filled"
                                />
                            </Grid>
                            <Divider flexItem style={{ margin: 5 }}/>
                            <Grid item>
                                <TextField
                                    disabled
                                    label="Encoding (Locally Forged)"
                                    fullWidth
                                    multiline
                                    value={encoding}
                                    variant="filled"
                                    maxRows={5}
                                    color="secondary"
                                />
                            </Grid>
                            <Divider flexItem style={{ margin: 10 }}/>
                        </Grid>
                    </DialogContent>
                </Dialog>
            </ScopedCssBaseline>
        </ThemeProvider>
    );
}

const el = document.createElement('div');
el.id = 'tezos-operation-checker';
window.document.body.appendChild(el);
render(<DraggableDialog />, document.getElementById(el.id));
