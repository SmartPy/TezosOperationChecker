import { Container } from '@material-ui/core';
import * as React from 'react';
import { render } from 'react-dom';

// Local imports
import MainView from '../../views/MainView';

import './index.css';

const View = () => (
    <Container>
        <MainView />
    </Container>
);

render(<View />, window.document.querySelector('#app-container'));
