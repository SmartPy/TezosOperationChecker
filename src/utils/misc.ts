import { blake2b } from 'blakejs';
import { encode } from 'bs58';
import { TezosMessageCodec, TezosMessageUtils } from '../services/conseil';

export const getEncoding = (
    operationsString: string,
    blockHeader: string
): string => {
    try {
        const operations = JSON.parse(operationsString);
        if (Array.isArray(operations)) {
            return operations.reduce(
                (state, cur) =>
                    `${state}${TezosMessageCodec.encodeOperation(cur)}`,
                TezosMessageUtils.writeBranch(blockHeader)
            );
        }
    } catch(e) {
        // Don't log parsing errors
        console.debug(e)
    }
};

export const generateBlake2BHash = (rawOpHex: string) => {
    const rawOp = Buffer.concat([
        Buffer.of(0x03),
        Buffer.from(rawOpHex, 'hex'),
    ]);
    return encode(Buffer.from(blake2b(rawOp, null, 32)));
};
