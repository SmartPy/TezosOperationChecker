export enum keys {
    THEME = 'THEME',
    BLOCK_HEADER = 'BLOCK_HEADER',
    OPERATIONS_CONTENT = 'OPERATIONS_CONTENT',
}
