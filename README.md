# ![Logo](src/assets/img/icon-36.png "Logo") TezosOperationChecker

[Chrome Extension](https://chrome.google.com/webstore/detail/tezosoperationchecker/oocojependgglmnbdmmelleoolopahge)

[Firefox Extension](https://addons.mozilla.org/en-US/firefox/addon/tezosoperationchecker)

## Preview

![Preview](preview.png)

## Install
```bash
# Don't using npm
yarn
```

## Development mode

```bash
$ yarn start
# Go to chrome://extensions
# Enable dev mode on top right
# Load unpacked from the top left (Open build folder)
```

## Build for production

```bash
NODE_ENV=production yarn build
```

## Interacting with the extension

*Example*
```js
window.postMessage({
    source: "TezosOperationChecker",
    kind: "REQUEST",
    payload: {
        blockHash: "BKtKSRquKS94eeXHpt8Tw8M9GQTjQ8FMcEfL54feQsc8sCrHRLu",
        operations: [
            {
                "kind": "origination",
                ...
            },
            {
                "kind": "transaction",
                ...
            },
            ...
        ]
    }
}, "*");
```
